<!doctype HTML>
<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 3/4/2017
 * Time: 4:34 PM
 */
include('SongController.php');
include('ContentLoader.php');
?>
<html>
    <head>
        <title>Music</title>
        <link rel="stylesheet" type="text/css" href="css/theme.css">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="css/menu.css">
        <link rel="stylesheet" type="text/css" href="css/music.css">
        <?php ContentLoader::getIconLinks()?>
    </head>
    <body>


        <?php ContentLoader::menu();
        ContentLoader::facebookSDK();?>

        <header id="musicHeader">
            <h1 class="vertical_centered">Music Player</h1>
        </header>
        <div id="playerContainer">
            <canvas id="visualizer">

            </canvas>
            <div id="player1" class="aplayer"></div>
        </div>


        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/APlayer.min.js"></script>
        <?php ContentLoader::footer();
        ContentLoader::getStatCounter();
        $playlistID = 2;
        $requestedPlaylist = $_GET['p'];
        if($requestedPlaylist != null)
            $playlistID = $requestedPlaylist;
        ?>
        <script src="js/menu.js"></script>
        <script>
            new APlayer({
                element: document.getElementById('player1'),                       // Optional, player element
                narrow: false,                                                     // Optional, narrow style
                autoplay: false,                                                    // Optional, autoplay song(s), not supported by mobile browsers
                showlrc: 0,                                                        // Optional, show lrc, can be 0, 1, 2, see: ###With lrc
                mutex: true,                                                       // Optional, pause other players when this player playing
                theme: '#e6d0b2',                                                  // Optional, theme color, default: #b7daff
                mode: 'random',                                                    // Optional, play mode, can be `random` `single` `circulation`(loop) `order`(no loop), default: `circulation`
                preload: 'metadata',                                               // Optional, the way to load music, can be 'none' 'metadata' 'auto', default: 'auto'
                listmaxheight: '513px',                                             // Optional, max height of play list
                music: <?php SongController::getSongs($playlistID)?>
            });
        </script>
<!--        {'title': 'song', 'artist': 'artist', 'url':'Songs/empty_streets.mp3'}-->
<!--        <php SongController::getSongs()?>-->
    <script src="js/visualizeAudio.js"></script>
    </body>
</html>
