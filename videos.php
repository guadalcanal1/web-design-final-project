<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Videos</title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/colors.css">
    <link rel="stylesheet" type="text/css" href="css/menu.css">
    <link rel="stylesheet" type="text/css" href="css/theme.css">
    <link rel="stylesheet" type="text/css" href="css/videos.css">
    <?php include('ContentLoader.php'); ContentLoader::getIconLinks()?>
</head>
<body>
<?php
ContentLoader::menu();
ContentLoader::facebookSDK();
?>

<header>
    <h1 class="vertical_centered">Videos</h1>
</header>
<div class="videoContainer">
    <h2 class="videoTitle">Danimals Commercial</h2>
    <iframe width="560" height="315" src="https://www.youtube.com/embed/xeNcgycfNgg" frameborder="0" allowfullscreen></iframe>
    <p class="videoDescription">This is incredible. Wow. So incredible. Better like and share. Also you must subscribe.
    Wow. Just wow. So great. Wow.</p>
</div>
<div class="videoContainer">
    <h2 class="videoTitle">Danimals Commercial 2</h2>
    <iframe width="560" height="315" src="https://www.youtube.com/embed/s8kwNBvL4hI" frameborder="0" allowfullscreen></iframe>
    <p class="videoDescription">This is incredible. Wow. So incredible. Better like and share. Also you must subscribe.
    Wow. Just wow. So great. Wow.</p>
</div>

<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/menu.js"></script>
<?php
ContentLoader::footer();
ContentLoader::getStatCounter();
?>
</body>
