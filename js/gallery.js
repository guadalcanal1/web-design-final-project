/**
 * Created by Ian on 3/2/2017.
 */
var open = false;
function showImage(file){
    if(!open) {
        $('#overlay').removeClass('invisible');
        $('#galleryViewer').removeClass('invisible');
        $('#galleryImage').attr('src', file);
        open = true;
    }
}

$('#overlay').click(function(){
    close();
});

$(document).keyup(function(e) {
    if (e.keyCode == 27) { //Escape
        close();
    }
});

function close(){
    if(open){
        $('#overlay').addClass('invisible');
        $('#galleryViewer').addClass('invisible');
        open = false;
    }
}