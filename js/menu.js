/**
 * Created by Ian on 3/2/2017.
 */
$(window).resize(function(){
    adjustMenu();
});

var menuClicked = false;

function adjustMenu(){
    var w = $(window).width();
    var fullWidthNav = $('#fullWidthNav');
    var menuToggleButton = $("#menuToggle");
    if(w < 750){
        fullWidthNav.children().hide();
        menuToggleButton.show();
    }else{
        $(".bottomBar").hide();
        fullWidthNav.children().show();
        menuToggleButton.hide();
    }
}

$("#menuToggle").click(function(e){
   e.preventDefault();
   menuClicked = !menuClicked;
   if(menuClicked) {
       $('.bottomBar').show();
   }else {
       $('.bottomBar').hide();
   }
});

adjustMenu();