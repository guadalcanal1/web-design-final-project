/**
 * Created by Ian on 1/31/2017.
 */
document.addEventListener("load", canvasInit());
var c,ctx,width,height,control;
var starSpeed,stars,numStars;
var mouseX,mouseY;
var bkgColor = "#000000";
var mode = 0;
var modes = 7;
//var lineColor = "#74d9dd";


function canvasInit() {
    numStars = 10000;
    starSpeed = 25;
    stars = [];
    var FPS = 60;
    c = document.getElementById("playground_canvas");
    if(c!=null) {
        updateSize();
        mouseX = width / 2;
        mouseY = height / 2;
        c.addEventListener('mousemove', function (evt) {
            var rect = c.getBoundingClientRect();
            mouseX = evt.clientX - rect.left;
            mouseY = evt.clientY - rect.top;
        }, true);
        c.addEventListener('click', function (evt) {
            mode++;
            if(mode>=modes)
                mode = 0;
        }, true);
        ctx = c.getContext("2d");

        console.log('Stars: ' + numStars);
        var i;
        for (i = 0; i < numStars; i++)
            stars[i] = createStar();
        ctx.fillStyle = bkgColor;
        ctx.fillRect(0, 0, width, height);
        setInterval(function () {
            update();
            draw();
        }, 1000 / FPS);
    }
}

function getRandomHexColor(cap){
    var hexValues = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'a', 'b', 'c', 'd', 'e', 'f'];
    var ret = '#';
    var i;
    for(i=0; i<6; i++) {
        var r = Math.floor(Math.random()*cap);
        ret += hexValues[cap - 1 - r];
    }
    return ret;
}

function update(){
    updateSize();
    for(var i=0; i < numStars; i++){
        if(stars[i].isOutOfBounds()) {
            stars[i] = createStar();
        }
        stars[i].animate();
    }
}

function draw(){
    ctx.clearRect(0,0,width,height);
    ctx.fillStyle = bkgColor;
    ctx.fillRect(0,0,width,height);
    for(var i=0; i < numStars; i++){
        ctx.strokeStyle = stars[i].color;
        stars[i].draw(ctx);
    }
}

function updateSize(){
    c.width = c.parentNode.clientWidth;
    c.height = c.parentNode.clientHeight;
    width = c.width;
    height = c.height;
}

function Star(x, y, direction){
    this.x = x;
    this.y = y;
    this.lastX = x;
    this.lastY = y;
    this.direction = direction;
    this.color = getRandomHexColor(16);
    this.timer = 1 + Math.floor(Math.random() * 5);
    this.animate = function(){
        var x = this.x;
        var y = this.y;
        this.lastX = x;
        this.lastY = y;
        var d;
        switch(mode) {
            case 0: d = Math.atan2(x - mouseX, y - mouseY); break;
            case 1: d = Math.tan((x-mouseX)/(y-mouseY)); break;
            case 2: d= Math.sin((x-mouseX)/(y-mouseY)); break;
            case 3: d= Math.cos((x-mouseX)/(y-mouseY)); break;
            case 4: d = Math.atan2(mouseX - x, y - mouseY); break;
            case 5: d = Math.tan((mouseX-x)/(y-mouseY)); break;
            case 6: d = Math.atan((mouseX-x)/(y-mouseY)); break;
        }
        this.direction = d;
        var maxDistance = width/4;
        var currentDistance = Math.sqrt(Math.pow(x-mouseX,2)+Math.pow(y-mouseY,2));
        var relDistRatio = (maxDistance-currentDistance)/maxDistance;
        var speedMod = (Math.pow(relDistRatio,3) - 3 * Math.pow(relDistRatio,2))* Math.sign(maxDistance-currentDistance);
        var dx = Math.cos(this.direction) * starSpeed *speedMod;
        var dy = -Math.sin(this.direction) * starSpeed * speedMod;
        this.x += dx;
        this.y += dy;
    }.bind(this);
    this.isOutOfBounds = function(){
        if(this.timer > 0) {
            this.timer--;
            return false;
        }else {
            this.timer = 1 + Math.floor(Math.random() * 5);
            var x = this.x;
            var y = this.y;
            return (x < 0 || x > width || y < 0 || y > height);
        }
    }.bind(this);
    this.draw = function(canvas){
        var x = this.x;
        var y = this.y;
        var lastX = this.lastX;
        var lastY = this.lastY;
        canvas.beginPath();
        canvas.moveTo(lastX,lastY);
        canvas.lineTo(x,y);
        canvas.lineWidth = 2;
        canvas.stroke();
    }
}

function createStar(){
    var x = Math.random()*width;
    var y = Math.random()*height;
    var direction = Math.atan2(mouseX - x,y-mouseY);
    return new Star(x,y,direction);
}