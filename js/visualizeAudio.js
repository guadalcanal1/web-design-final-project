/**
 * Created by Ian on 3/4/2017.
 */

window.addEventListener('load', initVisual, false);

var canvas, context, width, height, squares;
var bkgColor = "#fff";
var numSquares = 10;
var minSize = 20;
var maxSize = 100;
var minRotSpeed = -.1;
var maxRotSpeed = .1;
var minSpeed = 4;
var maxSpeed = 10;
var FPS = 60;

function initVisual() {
    canvas = document.getElementById('visualizer');
    context = canvas.getContext('2d');
    width = canvas.parentNode.clientWidth;
    height = canvas.parentNode.clientHeight;
    context.fillStyle = bkgColor;
    context.fillRect(0, 0, width, height);
    squares = [];
    for (var i = 0; i < numSquares; i++)
        squares[i] = createSquare();
    setInterval(function () {
        update();
        draw();
    }, 1000 / FPS);
}

function update(){
    updateSize();
    for(var i=0; i < numSquares; i++){
        if(squares[i].isOutOfBounds()) {
            squares[i] = createSquare();
        }
        squares[i].update();
    }
}

function draw(){
    context.clearRect(0,0,width,height);
    context.fillStyle = bkgColor;
    context.fillRect(0,0,width,height);
    for(var i=0; i < numSquares; i++){
        squares[i].draw(context);
    }
}

function updateSize(){
    width = canvas.scrollWidth;
    height = canvas.scrollHeight;
    canvas.height = height;
    canvas.width = width;
}

function Square(x,y,size,rotSpeed,speed,color){
    this.trailLength = 15;
    this.x = x;
    this.y = y;
    this.size = size;
    this.rotSpeed = rotSpeed;
    this.direction = Math.random()*Math.PI*2;
    this.speed = speed;
    this.color = color;
    this.angle = this.direction;
    this.dx = 0;
    this.dy = 0;
    var sideList = [3,4,5];
    this.sides = sideList[Math.floor(Math.random() * sideList.length)];

    this.update = function(){
        var d = this.direction;
        var r = this.rotSpeed;
        var sp = this.speed;
        var dx = Math.cos(d) * sp;
        var dy = -Math.sin(d) * sp;
        this.dx = dx;
        this.dy = dy;
        this.x += dx;
        this.y += dy;
        this.angle += r;
    }.bind(this);
    this.draw = function(ctx){
        var x = this.x;
        var y = this.y;
        var d = this.angle;
        var s = this.size;
        var l = Math.sqrt(Math.pow(s,2)/2);
        var pi = Math.PI;
        ctx.strokeStyle = this.color;
        ctx.beginPath();
        var sides = this.sides;
        for(var i=0; i<sides; i++){
            var a = d+(pi/sides)*((i+1)*2-1);
            ctx.lineTo(x + Math.cos(a)*l,y - Math.sin(a)*l);
            //ctx.lineTo(x + Math.cos(a)*l,y - Math.sin(d+(pi/sides)*(i+1))*l); //Cool paper effect
        }
        ctx.closePath();
        ctx.fill();
        for(i=1;i<=this.trailLength;i++){
            ctx.beginPath();
            //ctx.strokeOpacity = i/this.trailLength;
            for(var n=0; n<sides; n++){
                a = d+(pi/sides)*((n+1)*2-1);
                ctx.lineTo(x-this.dx*i + Math.cos(a)*l,y-this.dy*i - Math.sin(a)*l);
            }
            ctx.closePath();
            ctx.stroke();
        }



        ctx.fillOpacity = 1;

    }.bind(this);
    this.isOutOfBounds = function(){
        var threshold = maxSize+10;
        var x = this.x;
        var y = this.y;
        return (x < -threshold || x > width+threshold || y < -threshold || y > height+threshold)
    }.bind(this);
}

function createSquare(){
    var x = Math.random()*width;
    var y = Math.random()*height;
    var color = getRandomHexColor(16);
    var rotSpeed = Math.random()*(maxRotSpeed-minRotSpeed)+minRotSpeed;
    var speed = Math.random()*(maxSpeed-minSpeed)+minSpeed;
    var size = Math.random()*(maxSize-minSize)+minSize;
    return new Square(x,y,size,rotSpeed,speed,color);
}

function getRandomHexColor(cap){
    var hexValues = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'a', 'b', 'c', 'd', 'e', 'f'];
    var ret = '#';
    var i;
    for(i=0; i<6; i++) {
        var r = Math.floor(Math.random()*cap);
        ret += hexValues[cap - 1 - r];
    }
    return ret;
}