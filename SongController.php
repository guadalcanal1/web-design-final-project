<?php

/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 3/4/2017
 * Time: 4:42 PM
 */
class SongController
{
    public static function getSongs($playlist){
        $db = new mysqli('localhost', 'project', 'testpass', 'final_project');
        //source -> http://codular.com/php-mysqli
        if($db->connect_errno > 0){
            die('Unable to connect to database [' . $db->connect_error . ']');
        }
        $pstmt = $db->prepare("SELECT `name`, `file`,`author` FROM `audio_file` where `playlist_id` = ?");
        $pstmt->bind_param("s", $playlist);
        $pstmt->execute();
        $pstmt->bind_result($name, $file, $author);
        echo '[';
        while($pstmt->fetch()){
            echo json_encode(['title'=>$name, 'url'=>'./Songs/'.$file, 'author'=>$author], true).',';
        }
        echo ']';
        $pstmt->free_result();
    }
}