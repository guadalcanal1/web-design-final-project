<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Haiku Generator</title>
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="css/colors.css">
        <link rel="stylesheet" type="text/css" href="css/menu.css">
        <link rel="stylesheet" type="text/css" href="css/theme.css">
        <?php include('ContentLoader.php'); ContentLoader::getIconLinks()?>
    </head>
    <body>
        <?php
        ContentLoader::menu();
        ContentLoader::facebookSDK();
        ?>

        <script>
            window.fbAsyncInit = function() {
                FB.init({
                    appId      : '751998494959730',
                    xfbml      : true,
                    version    : 'v2.8'
                });
                FB.AppEvents.logPageView();
            };
        </script>
        <section id="haiku">
            <div class="vertical_centered">
                <h1>Haiku Generator</h1>
                <p id="haikuContents"></p>
                <button onclick="newHaiku()">Generate</button>
                <button onclick="share()">Share on Facebook</button>
            </div>
        </section>
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/haiku.js"></script>
        <script src="js/menu.js"></script>
        <script>
            function share(){
                FB.ui({
                    method: 'feed',
                    link: 'http://ianstuff.com/haiku.php',
                    description: 'I made this Haiku: ',
                    caption: $('#haikuContents').text().replace(new RegExp('\n', 'g'),' - ')
                }, function(response){});
            }
        </script>
        <?php
        ContentLoader::footer();
        ContentLoader::getStatCounter();
        ?>
    </body>
</html>