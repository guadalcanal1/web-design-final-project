<!Doctype HTML>
<html>
    <head>
        <title>Cool Star Thing</title>
        <link rel="stylesheet" type="text/css" href="css/theme.css">
        <?php include('ContentLoader.php'); ContentLoader::getIconLinks()?>
    </head>
    <body>
    <section id="playground">
        <h1>Playground</h1>
        <canvas id="playground_canvas"></canvas>
    </section>
    <script src="js/playground.js"></script>
    </body>
</html>