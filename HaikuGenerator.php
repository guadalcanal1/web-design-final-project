<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 2/2/2017
 * Time: 1:03 PM
 */
class Haiku
{
    private static $firstLines = [
        "Flatulent sunbeams", "Endocrine System", "Eating, Flying, Fear", "Kumquat Squash Puree", "Excellence surprise", "Disguised fake posers",
        "Angry green people", "Free birds, free sorrow", "Freedom is pricey", "Justice cannot sink", "Incompatible", "Syringes, soup spoons", "The pit of the peach",
        "Despite falsities", "Pretzel M&Ms", "Darkness encroaches", "Potatoes and rice", "Unlocked computers", "Potato famine", "Grapefruit diet man"
    ];

    private static $secondLines = [
        "Always necessitating", "Within you but against you", "Cannot be remote controlled", "Consumed by the plebeians",
        "Hiding in birthday presents", "Because the slates are dirty", "Gastrointestinal fruit", "Cannot be saved by the French",
        "In Germany is attacked", "By the disgusting lemons", "Around the slimy parrot", "Is only the beginning", "Donald the incredible",
        "Dave the fantastic great one", "Can rapture lost souls from death", "The flying monkeys devour", "The greatest sacrifices",
        "Junk food all over the place", "With crippling depression"
    ];

    private static $thirdLines = [
        "Intestinal tract", "Cafeteria", "Charlatan parade", "AOL group chat", "Napster freedom rules", "Avoid government", "Embrace techno rap",
        "Gun smoke in Asia", "French fries without sauce", "Cool Level: Cuba", "North Korean rice", "PBS is life", "Minecraft beastmode life",
        "Grungy heart muffin", "Of great happiness", "Will crush infidels", "But at what great cost", "Is this what you want?", "How much can you take?",
        "Club Penguin is life", "For the Mormon church", "Jehova's Witness", "Slavic Wonderland", "Disney on steroids", "Antarctic desert"
    ];
    public static function makeHaiku(){
        $f = self::$firstLines;
        $s = self::$secondLines;
        $t = self::$thirdLines;
        return $f[array_rand($f)]."\n<br>"
        .$s[array_rand($s)]."\n<br>".$t[array_rand($t)];
    }
}
echo Haiku::makeHaiku();