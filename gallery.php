<!DOCTYPE html>
<html>
    <head>
        <title>Gallery</title>
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="css/menu.css">
        <link rel="stylesheet" type="text/css" href="css/theme.css">
        <link rel="stylesheet" type="text/css" href="css/gallery.css">
        <?php include('ContentLoader.php'); ContentLoader::getIconLinks()?>
    </head>
    <body>
        <?php
            ContentLoader::menu();
            ContentLoader::facebookSDK();
        ?>
        <section id="heading">
            <h1 class="vertical_centered">Photo Gallery</h1>
        </section>
        <div id="imageFocus">
            <div class = "invisible" id="galleryViewer">
                <img id="galleryImage" src="#" alt="Gallery Image"/>
            </div>
            <div id="overlay" class="invisible"></div>
        </div>
        <div id="gallery">
            <?php
            $path = './img/gallery_photos/thumbnail/';
            $fullSizePath = './img/gallery_photos/';
            $dir = opendir($path);
            while(($file = readdir($dir))) {
                $name = $path.$file;
                $fullSize = $fullSizePath.$file;
                if(is_file($name))
                    echo "<a onclick=\"showImage('$fullSize')\">
                <img src='$name' alt='$file'/>
              </a>";
            }
            ?>
        </div>
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/gallery.js"></script>
        <script src="js/menu.js"></script>
        <?php
        ContentLoader::footer();
        ContentLoader::getStatCounter();
        ?>
    </body>
</html>
