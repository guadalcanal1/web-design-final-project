<!DOCTYPE html>
<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 3/2/2017
 * Time: 6:25 PM
 */
?>
<html>
    <head>
        <title>Map Explorer</title>
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <link rel="stylesheet" href="css/theme.css" type="text/css">
        <link rel="stylesheet" href="css/menu.css" type="text/css">
        <?php include('ContentLoader.php'); ContentLoader::getIconLinks()?>
    </head>
    <body>
    <?php
        ContentLoader::menu();
        ContentLoader::facebookSDK();
    ?>
    <section id="google_map">
        <h1>Map Explorer</h1>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDLQqQktAd6JcQhMsV6wUeHFvWvnce7zgk"></script>
        <div id="gmap_controls">
            <div id="content">
                <input type="text" id="location_box" value="The Master's University, CA">
                <button id="location_search">Search</button>
            </div>
        </div>
        <div id="gmap">

        </div>
    </section>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/map.js"></script>
    <script src="js/menu.js"></script>
    <?php
    ContentLoader::footer();
    ContentLoader::getStatCounter();
    ?>
    </body>
</html>
