<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 3/8/2017
 * Time: 10:45 PM
 */

//Help from http://php.net/manual/en/features.file-upload.multiple.php

if(hash('ripemd160',$_POST['password']) != 'd9801519d0193a44fdeeb1b982264837c9cb4b64'){
//    echo 'Wrong password! redirecting...';
//    echo '<br>Received password: ' . $_POST['password'];
//    echo '<br>Hashed password: ' . hash('ripemd160',$_POST['password']);
//    echo '<br>Correct: ' . 'd9801519d0193a44fdeeb1b982264837c9cb4b64';
    //header("Location: addSong.php");
    die();
}

$songDir = 'Songs/';

function reArrayFiles($file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}
$db = new mysqli('localhost', 'project', 'testpass', 'final_project');
//source -> http://codular.com/php-mysqli
if($db->connect_errno > 0){
    die('Unable to connect to database [' . $db->connect_error . ']');
}


if ($_FILES['songs']) {
    $file_ary = reArrayFiles($_FILES['songs']);
    //var_dump($file_ary);
    foreach ($file_ary as $file) {
        switch( $file['error'] ) {
            case UPLOAD_ERR_OK:
                $message = false;;
                break;
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                $message .= ' - file too large (limit of some bytes).';
                break;
            case UPLOAD_ERR_PARTIAL:
                $message .= ' - file upload was not completed.';
                break;
            case UPLOAD_ERR_NO_FILE:
                $message .= ' - zero-length file uploaded.';
                break;
            default:
                $message .= ' - internal error #'.$file['error'];
                break;
        }
        if($message) {
            echo 'ERROR: ' . $message . '</br>';
            return;
        }
        $pattern = '/(.*)\.(.*)$/';
        $match = array();
        preg_match($pattern, $file['name'], $match);
        $name = $match[1];
        $extension = $match[2];
        $destFile = uniqid() . '.' . $extension;
        $target_Path = $songDir.basename( $destFile );
        move_uploaded_file($file['tmp_name'], $target_Path);
        $pstmt = $db->prepare("INSERT INTO `final_project`.`audio_file` (`file`, `name`, `author`, `playlist_id`) VALUES (?, ?, ?, '1');");
        $pstmt->bind_param("sss", $dbFile, $dbName, $dbAuthor);
        $dbFile = $destFile;
        $dbName = $name;
        $dbAuthor = 'a';
        $pstmt->execute();
    }
}
//var_dump($_FILES);

header("Location: music.php");
die();