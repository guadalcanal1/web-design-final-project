<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Past Projects</title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/colors.css">
    <link rel="stylesheet" type="text/css" href="css/menu.css">
    <link rel="stylesheet" type="text/css" href="css/theme.css">
    <link rel="stylesheet" type="text/css" href="css/projects.css">
    <?php include('ContentLoader.php'); ContentLoader::getIconLinks()?>
</head>
<body>
    <?php
    ContentLoader::menu();
    ContentLoader::facebookSDK();
    ?>

    <header>
        <h1 class="vertical_centered">Past Projects</h1>
    </header>
    <div class="project">
        <h2 class="projectTitle">HTML High 5</h2>
        <p class="projectDescription">HTML High 5 is a casual gaming website I created about a year ago.
            I made six games for the website. Each game has medals which can be earned, and high scores are saved.
        I made the site with Java for the server-side logic. The score submission system is actually quite complex
        to keep people from hacking it. See if you can break it!</p>
        <div class="projectImages">
            <a href="/img/high51.png" target="_blank"><img alt="screen shot" src="/img/high51.png"></a>
            <a href="/img/high52.png" target="_blank"><img alt="screen shot" src="/img/high52.png"></a>
        </div>
        <a class="seeMore" target="_blank" href="https://htmlhigh5.com/index.jsp?from=project">Visit HTMLHigh5.com!</a>
    </div>
    <div class="project">
        <h2 class="projectTitle">Fortress Duel</h2>
        <p class="projectDescription">Fortress Duel is a game that I spent a long time developing. I based
        it off a game I enjoyed playing when I was younger, called Age of War. Unfortunately I am not as
        artistically talented as the creator of that game. However, I was able to have an email conversation
        with the developer, which was cool for me. Overall I think this is the best game that I have completed as
        far as mechanics go. There is some strategy but it still moves along.</p>
        <div class="projectImages">
            <a href="/img/duel1.jpg" target="_blank"><img alt="screen shot" src="/img/duel1.jpg"></a>
            <a href="/img/duel2.jpg" target="_blank"><img alt="screen shot" src="/img/duel2.jpg"></a>
            <a href="/img/duel3.jpg" target="_blank"><img alt="screen shot" src="/img/duel3.jpg"></a>
            <a href="/img/duel4.jpg" target="_blank"><img alt="screen shot" src="/img/duel4.jpg"></a>
        </div>
        <a class="seeMore" download href="Files/Fortress-Duel-0.1.0.exe">Download Fortress Duel</a>
    </div>
    <div class="project">
        <h2 class="projectTitle">Naval Warfare</h2>
        <p class="projectDescription">Naval Warfare is by far the most complex game I have ever worked on. Sadly
        it is so complex that it remains unfinished. However, I still think the game mechanics are enjoyable
        and the graphics are not very bad considering the source. I spent hundreds of hours on this project,
        and its codebase is huge. This project taught me a ton about managing large programs.</p>
        <div class="projectImages">
            <a href="/img/naval1.png" target="_blank"><img alt="screen shot" src="/img/naval1.png"></a>
            <a href="/img/naval2.png" target="_blank"><img alt="screen shot" src="/img/naval2.png"></a>
            <a href="/img/naval3.png" target="_blank"><img alt="screen shot" src="/img/naval3.png"></a>
            <a href="/img/naval4.png" target="_blank"><img alt="screen shot" src="/img/naval4.png"></a>
        </div>
        <a class="seeMore" href="Files/Naval_Battle.exe" download>Download Naval Warfare</a>
    </div>

    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/menu.js"></script>
    <?php
    ContentLoader::footer();
    ContentLoader::getStatCounter();
    ?>
</body>
