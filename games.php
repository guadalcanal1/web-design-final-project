<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Games</title>
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="css/colors.css">
        <link rel="stylesheet" type="text/css" href="css/menu.css">
        <link rel="stylesheet" type="text/css" href="css/theme.css">
        <?php include('ContentLoader.php'); ContentLoader::getIconLinks()?>
    </head>
    <body>
    <?php
        ContentLoader::menu();
        ContentLoader::facebookSDK();
    ?>
    <div id="gamesPage">
        <section id="games">
            <h1 class="vertical_centered">
                Video Games
            </h1>
        </section>
        <a href="playground.php" target="_blank">
            <div class="game">
                <h2>Color Playground</h2>
                <p>Move the mouse to manipulate the varicolored particles swirling around the screen. Click to change the pattern.</p>
            </div>
        </a>
        <a href="haiku.php">
            <div class="game">
                <h2>Haiku Generator</h2>
                <p>Load random Haikus to inspire you and elevate your consciousness beyond our galaxy.</p>
            </div>
        </a>
        <a href="map.php">
            <div class="game">
                <h2>Map Explorer Tool</h2>
                <p>Search for locations on the map. That's all there is to it.</p>
            </div>
        </a>
    </div>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/menu.js"></script>
    <?php
    ContentLoader::footer();
    ContentLoader::getStatCounter();
    ?>
    </body>
</html>