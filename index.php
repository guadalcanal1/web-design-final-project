<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Ian's Website</title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/colors.css">
    <link rel="stylesheet" type="text/css" href="css/menu.css">
    <link rel="stylesheet" type="text/css" href="css/theme.css">
    <?php include('ContentLoader.php'); ContentLoader::getIconLinks()?>
</head>
<body>
    <?php
        ContentLoader::menu();
        ContentLoader::facebookSDK();
    ?>
    <header>
        <section id="top">
            <div id="text" class="vertical_centered">
                <h1>Welcome to Ian's Page</h1>
                <h2>Enjoy Your Stay!</h2>
            </div>
        </section>
    </header>
    <section class="parallax" id="photos">
        <div id="overlay">
            <h2 class="vertical_centered"><a href="gallery.php">Photo Gallery</a></h2>
        </div>
    </section>
    <section id="about">
        <div id="headers">
            <h2>About Me</h2>
            <h3>Hobbies:</h3>
        </div>
        <div id="hobbies">
            <div class="card">
                <div class="front">
                    <img src="/img/soccer.jpg" alt="Soccer ball on grassy field"/>
                    <div class="caption">
                        <h4>Soccer</h4>
                    </div>
                </div>
                <div class="back">
                    <div class="vertical_centered">
                        <p>
                            I have been playing soccer since the age of 5.
                        </p>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="front">
                    <img src="/img/programming.jpg" alt="Photograph of code"/>
                    <div class="caption">
                        <h4>Programming</h4>
                    </div>
                </div>
                <div class="back">
                    <div class="vertical_centered">
                        <p>
                            I started programming when I was 13.
                        </p>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="front">
                    <img src="/img/rc_car.jpg" alt="Remote control car"/>
                    <div class="caption">
                        <h4>RC Cars</h4>
                    </div>
                </div>
                <div class="back">
                    <div class="vertical_centered">
                        <p>
                            Possibly my lamest hobby.
                        </p>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="front">
                    <img src="/img/tanks.jpg" alt="Charging tanks"/>
                    <div class="caption">
                        <h4>Tank Games</h4>
                    </div>
                </div>
                <div class="back">
                    <div class="vertical_centered">
                        <p>
                            Shoutout to everyone for making fun of my tank games.
                        </p>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="front">
                    <img src="/img/genealogy.jpg" alt="Genealogical research"/>
                    <div class="caption">
                        <h4>Genealogical Research</h4>
                    </div>
                </div>
                <div class="back">
                    <div class="vertical_centered">
                        <p>
                            I have been researching my ancestry for several months.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/menu.js"></script>
    <?php
    ContentLoader::footer();
    ContentLoader::getStatCounter();
    ?>
</body>
</html>