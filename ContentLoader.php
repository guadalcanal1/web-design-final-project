<?php

/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 1/29/2017
 * Time: 11:27 PM
 */
class ContentLoader
{
    private static $js = ['jquery-3.1.1.min.js', 'scripts.js', 'playground.js', 'haiku.js', 'map.js'];
    private static $css = ['variables.css', 'theme.css', 'menu.css'];

    public static function getCssImports() {
        $imports = '';
        foreach(self::$css as $file){
            $imports .= '<link type="text/css" rel="stylesheet" href="/css/' . $file . '">';
        }
        return $imports;
    }

    public static function menu(){
        $links = '<a href="/index.php">Home</a>
                        <a href="/games.php">Games</a>
                        <a href="/projects.php">Projects</a>
                        <a href="/gallery.php">Photos</a>
                        <a href="/music.php">Music</a>
                        <a href="/videos.php">Videos</a>';
        echo '<div id="menu">
            <div class="topBar">
                <a class = "title vertical_centered" href="/index.php"> Ian\'s Website </a>
                <nav id="fullWidthNav" class="vertical_centered">
                    ' . $links . '
                </nav>
                <a href="#" id="menuToggle" class="vertical_centered">Menu</a>
            </div>
            <div class="bottomBar">
                <nav>
                    ' . $links . '
                </nav>
            </div>
        </div>';
    }

    public static function facebookSDK(){
        echo '<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=751998494959730";
  fjs.parentNode.insertBefore(js, fjs);
}(document, \'script\', \'facebook-jssdk\'));</script>';
    }

    public static function footer(){
        echo '<div id="footer">
        <div class="vertical_centered">
            <p>Property of Ian Andersen. Sponsored by
                <a target="_blank" href="https://htmlhigh5.com/index.jsp?ref=project" title="Free HTML5 Games">
                    <img alt="HTML High 5" src="/img/logo_htmlhigh.jpg">
                </a>
            </p>
            <div class="fb-like" data-href="https://ianstuff.com" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
        </div>
    </div>';
    }

    public static function getJsImports(){
        $imports = '';
        foreach(self::$js as $file){
            $imports .= '<script src="/js/' . $file . '"></script>';
        }
        return $imports;
    }

    public static function getIconLinks(){
        echo '<link rel="apple-touch-icon" sizes="57x57" href="/icon/apple-touch-icon-57x57.png" />
                <link rel="apple-touch-icon" sizes="114x114" href="/icon/apple-touch-icon-114x114.png" />
                <link rel="apple-touch-icon" sizes="72x72" href="/icon/apple-touch-icon-72x72.png" />
                <link rel="apple-touch-icon" sizes="144x144" href="/icon/apple-touch-icon-144x144.png" />
                <link rel="apple-touch-icon" sizes="60x60" href="/icon/apple-touch-icon-60x60.png" />
                <link rel="apple-touch-icon" sizes="120x120" href="/icon/apple-touch-icon-120x120.png" />
                <link rel="apple-touch-icon" sizes="76x76" href="/icon/apple-touch-icon-76x76.png" />
                <link rel="apple-touch-icon" sizes="152x152" href="/icon/apple-touch-icon-152x152.png" />
                <link rel="icon" type="image/png" href="/icon/favicon-196x196.png" sizes="196x196" />
                <link rel="icon" type="image/png" href="/icon/favicon-96x96.png" sizes="96x96" />
                <link rel="icon" type="image/png" href="/icon/favicon-32x32.png" sizes="32x32" />
                <link rel="icon" type="image/png" href="/icon/favicon-16x16.png" sizes="16x16" />
                <link rel="icon" type="image/png" href="/icon/favicon-128.png" sizes="128x128" />
                <meta name="application-name" content="Ian\'s Stuff"/>
                <meta name="msapplication-TileColor" content="#FFFFFF" />
                <meta name="msapplication-TileImage" content="/icon/mstile-144x144.png" />
                <meta name="msapplication-square70x70logo" content="/icon/mstile-70x70.png" />
                <meta name="msapplication-square150x150logo" content="/icon/mstile-150x150.png" />
                <meta name="msapplication-wide310x150logo" content="/icon/mstile-310x150.png" />
                <meta name="msapplication-square310x310logo" content="/icon/mstile-310x310.png" />
                <meta property="og:url"           content="http://www.ianstuff.com/" />
                  <meta property="og:type"          content="website" />
                  <meta property="og:title"         content="Ian\'s Website" />
                  <meta property="og:description"   content="The website Ian made for his final project in web design." />
                  <meta property="og:image"         content="http://www.ianstuff.com/icon/mstile-310x310.png" />
            ';
    }

    public static function getStatCounter(){
        $blacklist = array(
            '127.0.0.1',
            '::1'
        );
        //Got this idea from http://stackoverflow.com/questions/2053245/how-can-i-detect-if-the-user-is-on-localhost-in-php
        if(!in_array($_SERVER['REMOTE_ADDR'], $blacklist)){
            echo '<!-- Start of StatCounter Code for Default Guide -->
            <script type="text/javascript">
            var sc_project=11277936; 
            var sc_invisible=1; 
            var sc_security="c2ceff62"; 
            var scJsHost = (("https:" == document.location.protocol) ?
            "https://secure." : "http://www.");
            document.write("<sc"+"ript type=\'text/javascript\' src=\'" +
            scJsHost+
            "statcounter.com/counter/counter.js\'></"+"script>");
            </script>
            <noscript><div class="statcounter"><a title="web stats"
            href="/icon/http://statcounter.com/" target="_blank"><img
            class="statcounter"
            src="//c.statcounter.com/11277936/0/c2ceff62/1/" alt="web
            stats"></a></div></noscript>
            <!-- End of StatCounter Code for Default Guide -->';
        }

    }

    public static function getAllImports(){
        return self::getJsImports().self::getCssImports();
    }
}